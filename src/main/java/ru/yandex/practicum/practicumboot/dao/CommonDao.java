package ru.yandex.practicum.practicumboot.dao;

import java.util.List;

public interface CommonDao<T> {

    Long create(T entity);

    T getById(Long id);

    List<T> getAll();

    void update(T entity);

    void delete(Long id);

}
