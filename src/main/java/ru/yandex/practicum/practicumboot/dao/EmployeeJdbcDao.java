package ru.yandex.practicum.practicumboot.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;
import ru.yandex.practicum.practicumboot.model.EmployeeModel;

import java.sql.PreparedStatement;
import java.util.List;

@Component
public class EmployeeJdbcDao implements CommonDao<EmployeeModel> {

    private static final String GET_ALL = "select id, first_name, last_name, salary from employee";
    private static final String GET_BY_ID = """
        select
            id, first_name, last_name, salary
        from
            employee e
        where
            id = ?
    """;
    private static final String CREATE = "insert into employee(first_name, last_name, salary)\n" +
            "values (?, ?, ?)";

    private static final String UPDATE = """
        update employee
        set
            first_name = ?,
            last_name = ?,
            salary = ?
        where
            id = ?
    """;

    private static final String DELETE = "delete from employee where id = ?";

    private static final RowMapper<EmployeeModel> mapper = (result, rowNum) -> {
        Long id = result.getLong("id");
        String firstName = result.getString("first_name");
        String lastName = result.getString("last_name");
        int salary = result.getInt("salary");
        return new EmployeeModel(id, firstName, lastName, salary);
    };

    private final JdbcOperations jdbcOperations;

    @Autowired
    public EmployeeJdbcDao(JdbcOperations jdbcOperations) {
        this.jdbcOperations = jdbcOperations;
    }

    @Override
    public Long create(EmployeeModel entity) {
        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcOperations.update(con -> {
            PreparedStatement ps = con.prepareStatement(CREATE, PreparedStatement.RETURN_GENERATED_KEYS);
            ps.setString(1, entity.getFirstName());
            ps.setString(2, entity.getLastName());
            ps.setInt(3, entity.getSalary());
            return ps;
        }, keyHolder);

        Long newId;
        if (keyHolder.getKeys().size() > 1) {
            newId = (Long)keyHolder.getKeys().get("id");
        } else {
            newId= keyHolder.getKey().longValue();
        }

        return newId;
    }

    @Override
    public EmployeeModel getById(Long id) {
        return jdbcOperations.queryForObject(GET_BY_ID, mapper, id);
    }

    @Override
    public List<EmployeeModel> getAll() {
        return jdbcOperations.query(GET_ALL, mapper);
    }

    @Override
    public void update(EmployeeModel entity) {
        jdbcOperations.update(UPDATE,
                entity.getFirstName(),
                entity.getLastName(),
                entity.getSalary(),
                entity.getId());
    }

    @Override
    public void delete(Long id) {
        jdbcOperations.update(DELETE, id);
    }
}
