package ru.yandex.practicum.practicumboot.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class EmployeeModel {

    private Long id;

    private String firstName;
    private String lastName;
    private int salary;

    public EmployeeModel(String firstName, String lastName, int salary) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.salary = salary;
    }
}
