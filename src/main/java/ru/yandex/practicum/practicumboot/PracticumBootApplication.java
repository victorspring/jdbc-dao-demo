package ru.yandex.practicum.practicumboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.JdbcTemplate;

@SpringBootApplication
public class PracticumBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(PracticumBootApplication.class, args);
	}



}
