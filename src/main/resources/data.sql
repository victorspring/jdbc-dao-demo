drop table if exists employee;
drop table if exists department;

create table department(
    id bigserial primary key,
    title varchar(50)
);

create table employee(
	id bigserial primary key,
	first_name varchar(50),
	last_name varchar(50),
	department_id bigint references department(id),
	salary int
);

insert into department(title) values ('IT');
insert into department(title) values ('Management');

insert into employee(first_name, last_name, department_id, salary)
	values ('Bill', 'Gates', 1, 100);
insert into employee(first_name, last_name, department_id, salary)
	values ('Elon', 'Musk', 2, 150);
insert into employee(first_name, last_name, department_id, salary)
	values ('Jeff ', 'Bezos', 2, 200);
