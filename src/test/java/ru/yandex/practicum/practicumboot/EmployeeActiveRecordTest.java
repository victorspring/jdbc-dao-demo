package ru.yandex.practicum.practicumboot;

import com.tej.JooQDemo.jooq.sample.model.Tables;
import com.tej.JooQDemo.jooq.sample.model.tables.records.EmployeeRecord;
import org.jooq.DSLContext;
import org.jooq.Result;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@Sql(scripts = "classpath:data.sql",
		executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
class EmployeeActiveRecordTest {

	private final DSLContext context;

	@Autowired
	EmployeeActiveRecordTest(DSLContext context) {
		this.context = context;
	}


	@Test
	void updateEmployees() {
		Result<EmployeeRecord> employees =  selectEmployeesFromManagement();
		employees.forEach(employee -> employee.set(Tables.EMPLOYEE.SALARY, 1000));
		context.batchStore(employees).execute();

		selectEmployeesFromManagement().forEach(employee -> assertEquals(1000, employee.getSalary()));

	}

	private Result<EmployeeRecord> selectEmployeesFromManagement(){
		return context
				.select()
				.from(Tables.EMPLOYEE
						.innerJoin(Tables.DEPARTMENT)
							.on(Tables.EMPLOYEE.DEPARTMENT_ID.eq(Tables.DEPARTMENT.ID))
						.where(Tables.DEPARTMENT.TITLE.equal("Management")))
				.fetchInto(Tables.EMPLOYEE);
	}



}
