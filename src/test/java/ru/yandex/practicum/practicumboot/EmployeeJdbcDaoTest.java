package ru.yandex.practicum.practicumboot;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import ru.yandex.practicum.practicumboot.dao.CommonDao;
import ru.yandex.practicum.practicumboot.model.EmployeeModel;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@Sql(scripts = "classpath:data.sql",
		executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
class EmployeeJdbcDaoTest {

	private final CommonDao<EmployeeModel> employeeDao;

	@Autowired
	EmployeeJdbcDaoTest(CommonDao<EmployeeModel> employeeDao) {
		this.employeeDao = employeeDao;
	}

	@Test
	void create() {
		EmployeeModel employee = new EmployeeModel("Victor", "Bodrov", 1000);
		Long id = employeeDao.create(employee);
		assertEquals(4, id);

		List<EmployeeModel> employees = employeeDao.getAll();
		assertEquals(4, employees.size());

		EmployeeModel newEmployee = employeeDao.getById(4L);
		assertEquals(employee.getFirstName(), newEmployee.getFirstName());
	}

	@Test
	void delete() {
		employeeDao.delete(1L);
		assertEquals(2, employeeDao.getAll().size());
	}


}
